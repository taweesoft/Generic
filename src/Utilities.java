import java.util.Arrays;


/**
 * Utilities class.
 * @author taweesoft
 * 16.05.2015.
 */
public class Utilities {
	
	/**
	 * Find the maximum value of parameters.
	 * Generic type must be a something that a path of Comparable.
	 * So we use wildcard as E extends Comparable<? extends E>;
	 * Addition : We use ? extends E in Comparable genetic type because
	 * we can find compare from something that inherit from E too.
	 * @param args
	 * @return
	 */
	public static <E extends Comparable<? extends E>> E max(E ... args){
		System.out.println("Here : max with comparable generic type");
		Arrays.sort(args);
		return args[args.length-1];
	}
	
	public static <E> E max(E ... args){
		System.out.println("Here : max with any generic type");
		try{
			Arrays.sort(args);
		}catch(ClassCastException e){System.out.println("Class can not cast");}
		return args[args.length-1];
	}
	
	/**
	 * Find the average of parameters.
	 * Genetic type must be a Number or a path of Number.
	 * So we will use wildcard as E extends Number
	 * @param args
	 * @return
	 */
	public static <E extends Number> double average(E ... args){
		double e = 0;
		for(E n : args) e += n.doubleValue();
		return e/args.length;
	}
}
