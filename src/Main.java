import java.util.ArrayList;


/**
 * Main application
 * @author taweesoft
 * 16.05.2015.
 */
public class Main {
	public static void main(String[] args) {
		System.out.println("------Find the maximum value------");
		/*Find the maximum value.*/
		System.out.println(Utilities.max(4.8,4.3,5.8,5.6,10.0,5.4,11.5,6.7));
		
		//If you are uncomment this code below it will error because new ArrayList() can not sorted.
		System.out.println(Utilities.max(4.8,4.3,5.8,5.6,10.0,5.4,11.5,6.7,new ArrayList()));
		
		
		System.out.println("------Find the last word------");
		/*Find the last word by use dictionary's rule*/
		System.out.println(Utilities.max("apple","boat","zebra","alpha","cat","ZOO"));
		
		System.out.println("------Find the average of values------");
		/*Find the average of values.*/
		System.out.println(Utilities.average(4.5f,5.5,4,5l,7d,40,45.6,67,8));
	}
}
